const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const repassword = document.getElementById('repassword');

form.addEventListener('submit', (e) => {
  e.preventDefault();

  checkInputs();
})

const checkInputs = () => {
  const usernameValue = username.value.trim();
  const emailValue = email.value.trim();
  const passwordValue = password.value.trim();
  const repasswordValue = repassword.value.trim();

  if (usernameValue === '') {
    setErrorMsg(username, 'User cannot be empty');
  } else {
    setSuccessMsg(username);
  }

  if (emailValue === '') {
    setErrorMsg(email, 'Email cannot be empty');
  } else if (!isEmail(emailValue)) {
    setErrorMsg(email, 'Email is not valid');
  } else {
    setSuccessMsg(email);
  }

  if (passwordValue === '') {
    setErrorMsg(password, 'Password cannot be empty')
  } else {
    setSuccessMsg(password)
  }

  if (repasswordValue === '') {
    setErrorMsg(repassword, 'Re password cannot be empty')
  } else if (passwordValue !== repasswordValue) {
    setErrorMsg(repassword, 'Password does not match')
  } else {
    setSuccessMsg(repassword)
  }
}

const setErrorMsg = (input, message) => {
  const formControl = input.parentElement;
  const small = formControl.querySelector('small');

  small.innerText = message;
  formControl.className = 'form-control error';
}

const setSuccessMsg = (input) => {
  const formControl = input.parentElement;
  formControl.className = 'form-control success';
}

const isEmail = (email) => {
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
}
